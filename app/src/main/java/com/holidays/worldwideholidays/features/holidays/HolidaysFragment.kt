package com.holidays.worldwideholidays.features.holidays

import android.Manifest
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.holidays.worldwideholidays.R
import com.holidays.worldwideholidays.common.Constants.Companion.DELAY_SCROLL_TO_TOP
import com.holidays.worldwideholidays.common.utils.DialogUtils.promptEnableGps
import com.holidays.worldwideholidays.common.utils.DialogUtils.showLocationRationaleDialog
import com.holidays.worldwideholidays.common.utils.PermissionUtil.hasPermission
import com.holidays.worldwideholidays.common.utils.PermissionUtil.isGpsEnabled
import com.holidays.worldwideholidays.common.utils.PermissionUtil.isLocationPermissionDenied
import com.holidays.worldwideholidays.databinding.FragmentHolidaysBinding
import com.holidays.worldwideholidays.features.holidays.adapter.HolidaysAdapter
import com.holidays.worldwideholidays.features.holidays.adapter.HolidaysAdapterListener
import com.holidays.worldwideholidays.features.main.MainViewModel
import com.holidays.worldwideholidays.models.Holiday
import com.holidays.worldwideholidays.models.ListData
import com.holidays.worldwideholidays.models.ListState
import com.holidays.worldwideholidays.models.Status
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

/**
 * Fragment displaying a list of holidays and providing filtering options.
 */
class HolidaysFragment : Fragment() {

    // Shared ViewModel with the main activity
    private val sharedViewModel by activityViewModel<MainViewModel>()

    // ViewModel for the holidays fragment
    private val viewModel: HolidaysViewModel by viewModel()

    // View binding instance for the holidays fragment
    private var _binding: FragmentHolidaysBinding? = null
    private val binding get() = _binding!!

    // Adapter for the holidays list
    private lateinit var holidaysAdapter: HolidaysAdapter

    // Permission request handler for location permission
    private val permissionRequest =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            if (granted) {
                checkPermissions()
            } else {
                Timber.tag(TAG).d(getString(R.string.location_permission_denied))
            }
        }

    // Listener for holiday item interactions
    private val adapterListener =
        object : HolidaysAdapterListener {
            override fun onItemClick(item: Holiday) {
                sharedViewModel.openHoliday(item)
            }

            override fun onFavoriteClick(data: Holiday) {
                viewModel.updateFavoriteStatus(data)
            }
        }

    /**
     * Called when the fragment view is created.
     * Sets up the layout, adapter, and observes ViewModel data.
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHolidaysBinding.inflate(inflater, container, false)
        return binding.root
    }

    /**
     * Called when the fragment is resumed.
     * Checks permissions related to location and initiates fetching of holidays.
     */
    override fun onResume() {
        super.onResume()
        checkPermissions()
    }

    /**
     * Called when the fragment view is destroyed.
     * Clears the view binding instance to avoid memory leaks.
     */
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Called when the fragment view is created.
     * Initializes UI elements and sets up the adapter.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        initAdapter()
        initObservers()
    }

    /**
     * Initializes UI elements.
     */
    private fun initUi() {
        binding.apply {
            holidaysTab.setOnClickListener { viewModel.setHolidaysUIState() }
            favoriteHolidaysTab.setOnClickListener { viewModel.setFavoriteHolidaysUIState() }
        }
    }

    /**
     * Initializes the RecyclerView adapter.
     */
    private fun initAdapter() {
        binding.recyclerView.apply {
            holidaysAdapter = HolidaysAdapter(
                adapterListener
            )
            adapter = holidaysAdapter
        }
    }

    /**
     * Initializes observers for LiveData updates from the ViewModel.
     */
    private fun initObservers() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    viewModel.listState.collect {
                        handleListState(it)
                    }
                }
                launch {
                    viewModel.holidaysData.collect { uiState ->
                        when (uiState.status) {
                            Status.SUCCESS -> {
                                handleProgressBar(false)
                                handleData(uiState.data)
                            }

                            Status.ERROR -> {
                                handleProgressBar(false)
                                showError(uiState.message)
                            }

                            Status.LOADING -> handleProgressBar(true)
                            Status.EMPTY -> {}
                        }
                    }
                }
            }
        }
    }

    /**
     * Handles changes in the list state and updates UI accordingly.
     */
    private fun handleListState(it: ListState) {
        when (it) {
            ListState.HOLIDAYS -> {
                binding.holidaysTab.isSelected = true
                binding.favoriteHolidaysTab.isSelected = false
            }

            ListState.FAVORITES -> {
                binding.holidaysTab.isSelected = false
                binding.favoriteHolidaysTab.isSelected = true
            }
        }
    }

    /**
     * Handles data updates from the ViewModel and updates the UI.
     */
    private fun handleData(data: ListData?) {
        data?.let {
            holidaysAdapter.submitList(it.holidays)
            checkScrollToTop(it)
            updateListState(it)
        }
    }

    /**
     * Updates the visibility of UI elements based on the list state.
     */
    private fun updateListState(it: ListData) {
        binding.apply {
            if (it.holidays.isNullOrEmpty() && !it.emptyListTitle.isNullOrEmpty()) {
                recyclerView.isVisible = false
                emptyListTitle.isVisible = true
                emptyListTitle.text = it.emptyListTitle
            } else {
                emptyListTitle.isVisible = false
                recyclerView.isVisible = true
            }
        }
    }

    /**
     * Checks whether to scroll the list to the top based on the data.
     */
    private fun checkScrollToTop(it: ListData) {
        if (it.scrollToTop && !it.holidays.isNullOrEmpty()) {
            Handler(Looper.getMainLooper()).postDelayed({
                binding.recyclerView.smoothScrollToPosition(0)
            }, DELAY_SCROLL_TO_TOP)
        }
    }

    /**
     * Handles the visibility of the progress bar based on the loading status.
     */
    private fun handleProgressBar(isLoading: Boolean) {
        binding.progressView.isVisible = isLoading
    }

    /**
     * Displays an error message in the form of a toast.
     */
    private fun showError(message: String?) {
        Toast.makeText(
            requireContext(), message, Toast.LENGTH_LONG
        ).show()
    }

    /**
     * Checks and requests necessary permissions for location access.
     */
    private fun checkPermissions() {
        when {
            !requireContext().isGpsEnabled() -> promptEnableGps(requireContext())

            !requireContext().hasPermission(Manifest.permission.ACCESS_FINE_LOCATION) -> requestLocationPermission()

            else -> viewModel.initiateLocationAndHolidaysFetching()
        }
    }

    /**
     * Requests location permission if necessary, showing a rationale dialog if needed.
     */
    private fun requestLocationPermission() {
        if (requireActivity().isLocationPermissionDenied()) {
            showLocationRationaleDialog(requireContext(), permissionRequest)
        } else {
            permissionRequest.launch(Manifest.permission.ACCESS_FINE_LOCATION)
        }
    }

    /**
     * Companion object holding a simple tag for logging purposes.
     */
    companion object {
        private val TAG = HolidaysFragment::class.java.simpleName
    }
}