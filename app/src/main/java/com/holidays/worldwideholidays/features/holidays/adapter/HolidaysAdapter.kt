package com.holidays.worldwideholidays.features.holidays.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.holidays.worldwideholidays.R
import com.holidays.worldwideholidays.databinding.ItemHolidayBinding
import com.holidays.worldwideholidays.models.Holiday

/**
 * Adapter class for displaying a list of holidays in a RecyclerView.
 *
 * @param listener The listener to handle item click and favorite click events.
 */
class HolidaysAdapter(
    private val listener: HolidaysAdapterListener
) : ListAdapter<Holiday, RecyclerView.ViewHolder>(HolidaysDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val itemBinding =
            ItemHolidayBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CategoryViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CategoryViewHolder).bind(listener, getItem(position))
    }
}

/**
 * Interface definition for a listener to handle item click and favorite click events in the HolidaysAdapter.
 */
interface HolidaysAdapterListener {
    fun onItemClick(item: Holiday)
    fun onFavoriteClick(data: Holiday)
}

/**
 * ViewHolder class for displaying holiday items in the RecyclerView.
 *
 * @param itemBinding The binding instance for the item layout.
 */
class CategoryViewHolder(private val itemBinding: ItemHolidayBinding) :
    RecyclerView.ViewHolder(itemBinding.root) {

    /**
     * Binds data and listener to the item view.
     *
     * @param listener The listener to handle item click and favorite click events.
     * @param data The Holiday data to be displayed.
     */
    fun bind(listener: HolidaysAdapterListener, data: Holiday) {
        itemBinding.apply {
            root.setOnClickListener { listener.onItemClick(data) }
            favoriteButton.setOnClickListener { listener.onFavoriteClick(data) }

            name.text = data.name
            date.text = data.date

            checkFavoriteState(data.isFavorite)
        }
    }

    /**
     * Checks and sets the state of the favorite icon based on the provided boolean value.
     *
     * @param checked The boolean value indicating whether the item is marked as a favorite.
     */
    private fun ItemHolidayBinding.checkFavoriteState(checked: Boolean?) {
        val drawable = when (checked) {
            true -> R.drawable.ic_favorite_selected
            else -> R.drawable.ic_favorite_unselected
        }

        favoriteButton.setImageResource(drawable)
    }
}

/**
 * DiffUtil callback for calculating the difference between two lists of holidays.
 */
private class HolidaysDiffCallback : DiffUtil.ItemCallback<Holiday>() {
    override fun areItemsTheSame(
        oldItem: Holiday,
        newItem: Holiday
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: Holiday, newItem: Holiday
    ): Boolean {
        return oldItem == newItem
    }
}