package com.holidays.worldwideholidays.features.holidays

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.holidays.worldwideholidays.R
import com.holidays.worldwideholidays.common.dispatchers.DispatcherProvider
import com.holidays.worldwideholidays.common.extensions.CommonExtensions.getHolidaysUiModels
import com.holidays.worldwideholidays.common.extensions.CommonExtensions.updateFavoriteHolidays
import com.holidays.worldwideholidays.data.DatabaseRepository
import com.holidays.worldwideholidays.data.HolidaysRepository
import com.holidays.worldwideholidays.data.LocationRepository
import com.holidays.worldwideholidays.models.Holiday
import com.holidays.worldwideholidays.models.ListData
import com.holidays.worldwideholidays.models.ListState
import com.holidays.worldwideholidays.models.ResponseState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.Calendar

/**
 * ViewModel class responsible for managing and providing holiday-related data.
 *
 * This ViewModel class is responsible for fetching and managing holiday data,
 * including fetching location, holiday data, and handling favorites.
 *
 * @property dispatcherProvider The provider for dispatchers used in coroutines.
 * @property resources The Android application resources.
 * @property locationRepository The repository for fetching location data.
 * @property databaseRepository The repository for database operations.
 * @property holidaysRepository The repository for fetching holiday data.
 */
class HolidaysViewModel(
    private val dispatcherProvider: DispatcherProvider,
    private val resources: Resources,
    private val locationRepository: LocationRepository,
    private val databaseRepository: DatabaseRepository,
    private val holidaysRepository: HolidaysRepository
) : ViewModel() {

    // State flow for the selected list state (holidays or favorites)
    private val _listState = MutableStateFlow(ListState.HOLIDAYS)
    val listState: StateFlow<ListState> = _listState.asStateFlow()

    // State flow for holiday data
    private val _holidaysData = MutableStateFlow<ResponseState<ListData>>(ResponseState.loading())
    val holidaysData: StateFlow<ResponseState<ListData>> = _holidaysData.asStateFlow()

    /**
     * Initiates the process to fetch location and holiday data.
     */
    fun initiateLocationAndHolidaysFetching() {
        Timber.tag(TAG).d(::initiateLocationAndHolidaysFetching.name)
        viewModelScope.launch {
            try {
                // Fetch the current location
                val location = locationRepository.getLocations().first()
                val year = Calendar.getInstance().get(Calendar.YEAR)
                countryCode = locationRepository.getCountryCode(location)
                fetchHolidaysData(year, countryCode, true)
            } catch (e: Exception) {
                handleException(e)
            }
        }
    }

    /**
     * Fetches holiday data based on the specified year and country code.
     *
     * @param year The year for which to fetch holiday data.
     * @param countryCode The country code for which to fetch holiday data.
     * @param isUpdate Indicates whether to update existing data.
     */
    private fun fetchHolidaysData(year: Int, countryCode: String, isUpdate: Boolean) {
        Timber.tag(TAG).d(::fetchHolidaysData.name)
        viewModelScope.launch {
            try {
                // Loading state while fetching data
                _holidaysData.value = ResponseState.loading()

                // Fetch holiday data from the repository
                val result = holidaysRepository.fetchHolidays(year, countryCode, isUpdate).first()

                // Process and update UI with fetched data
                holidays = result.data?.getHolidaysUiModels()
                checkFavoriteHolidays()
            } catch (e: Exception) {
                handleException(e)
            }
        }
    }

    /**
     * Checks the user's favorite holidays and updates the UI.
     */
    private fun checkFavoriteHolidays() {
        Timber.tag(TAG).d(::checkFavoriteHolidays.name)
        viewModelScope.launch {
            try {
                // Fetch favorite holidays from the database
                val favorites = databaseRepository.getAllFavoriteHolidays().first()
                favoriteHolidays = favorites

                // Update holiday list with favorite status
                holidays = holidays?.updateFavoriteHolidays(favoriteHolidays)

                // Update the UI with the processed data
                updateListData(false)
            } catch (e: Exception) {
                handleException(e)
            }
        }
    }

    /**
     * Updates the favorite status of a holiday and refreshes the UI.
     *
     * @param holiday The holiday for which to update the favorite status.
     */
    fun updateFavoriteStatus(holiday: Holiday) {
        Timber.tag(TAG).d(::updateFavoriteStatus.name)
        viewModelScope.launch(dispatcherProvider.io) {
            try {
                // Toggle favorite status in the database
                when {
                    databaseRepository.isHolidayFavorite(holiday.id) ->
                        databaseRepository.removeFavoriteHoliday(holiday.id)

                    else -> databaseRepository.addFavoriteHoliday(holiday.copy(isFavorite = true))
                }

                // Check and update favorite holidays
                checkFavoriteHolidays()
            } catch (e: Exception) {
                // Handle errors and update UI
                _holidaysData.value = ResponseState.error(e.message.toString())
            }
        }
    }

    /**
     * Sets the UI state to display all holidays.
     */
    fun setHolidaysUIState() {
        Timber.tag(TAG).d(::setHolidaysUIState.name)
        _listState.value = ListState.HOLIDAYS
        updateListData(true)
    }

    /**
     * Sets the UI state to display only favorite holidays.
     */
    fun setFavoriteHolidaysUIState() {
        Timber.tag(TAG).d(::setFavoriteHolidaysUIState.name)
        _listState.value = ListState.FAVORITES
        updateListData(true)
    }

    /**
     * Updates the UI with the current holiday list and state.
     *
     * @param scrollToTop Indicates whether to scroll to the top of the list.
     */
    private fun updateListData(scrollToTop: Boolean) {
        var emptyListTitle: String? = null
        var list: List<Holiday>? = emptyList()

        when (_listState.value) {
            ListState.HOLIDAYS -> {
                list = holidays
                if (list.isNullOrEmpty()) emptyListTitle =
                    String.format(resources.getString(R.string.empty_holidays), countryCode)
            }

            ListState.FAVORITES -> {
                list = favoriteHolidays?.sortedBy { it.date }
                if (list.isNullOrEmpty()) emptyListTitle =
                    resources.getString(R.string.empty_favorites)
            }
        }

        Timber.tag(TAG).d("${::updateListData.name} list size: ${list?.size ?: 0}")

        _holidaysData.value =
            ResponseState.success(
                ListData(
                    holidays = list,
                    scrollToTop = scrollToTop,
                    emptyListTitle = emptyListTitle
                )
            )
    }

    /**
     * Handles exceptions and updates the UI state with an error status.
     *
     * @param e The exception that occurred.
     */
    private fun handleException(e: Throwable) {
        Timber.tag(TAG).e("${::handleException.name} failed: ${e.message}")
        _holidaysData.value = ResponseState.error(e.message.toString())
    }

    companion object {
        private val TAG = HolidaysViewModel::class.java.simpleName

        // Properties for maintaining state
        private var countryCode = ""
        private var holidays: List<Holiday>? = null
        private var favoriteHolidays: List<Holiday>? = null

    }
}
