package com.holidays.worldwideholidays.features.holidayDetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.holidays.worldwideholidays.R
import com.holidays.worldwideholidays.databinding.FragmentHolidayDetailsBinding
import com.holidays.worldwideholidays.models.Holiday

/**
 * Fragment class to display details of a selected holiday.
 */
class HolidayDetailsFragment : Fragment() {

    // View binding instance for the fragment
    private var binding: FragmentHolidayDetailsBinding? = null

    // Safe Args to retrieve arguments passed to this fragment
    private val args by navArgs<HolidayDetailsFragmentArgs>()

    /**
     * Inflates the fragment's view, initializes the binding, and returns the root view.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHolidayDetailsBinding.inflate(inflater, container, false)
        return binding?.root
    }

    /**
     * Clears the binding instance when the view is destroyed to avoid memory leaks.
     */
    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    /**
     * Handles the creation of the fragment's view.
     * Retrieves holiday details from arguments and updates the UI.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleHolidayDetails(args.item)
    }

    /**
     * Populates the UI with details of the selected holiday.
     *
     * @param data The Holiday object containing details to be displayed.
     */
    private fun handleHolidayDetails(data: Holiday?) {
        binding?.apply {
            data?.let {
                name.text = String.format(getString(R.string.name), it.name)
                localName.text = String.format(getString(R.string.local_name), it.localName)
                date.text = String.format(getString(R.string.date), it.date)
                countryCode.text = String.format(getString(R.string.country_code), it.countryCode)
                fixed.text = String.format(getString(R.string.fixed), it.fixed)
                global.text = String.format(getString(R.string.global), it.global)
                counties.text = String.format(getString(R.string.counties), it.counties)
                launchYear.text = String.format(getString(R.string.launch_year), it.launchYear)
                types.text = String.format(getString(R.string.type), it.types)
            }
        }
    }
}