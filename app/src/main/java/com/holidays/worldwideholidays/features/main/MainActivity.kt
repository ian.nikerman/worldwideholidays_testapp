package com.holidays.worldwideholidays.features.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.holidays.worldwideholidays.R
import com.holidays.worldwideholidays.databinding.ActivityMainBinding
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Main activity hosting the holiday-related fragments and navigation.
 */
class MainActivity : AppCompatActivity() {

    // View model for the main activity
    private val viewModel: MainViewModel by viewModel()

    // View binding instance for the main activity
    private lateinit var binding: ActivityMainBinding

    // AppBarConfiguration instance for setting up action bar navigation
    private lateinit var appBarConfiguration: AppBarConfiguration

    /**
     * Called when the activity is first created.
     * Sets up the layout, navigation, and observers.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Initialize observers for navigation events
        initObservers()

        // Set up navigation controller and action bar configuration
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.navigationContainer) as NavHostFragment
        val navController = navHostFragment.navController
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)
    }

    /**
     * Initializes observers for handling navigation events.
     */
    private fun initObservers() {
        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    viewModel.navigationEvent.collect {
                        handleNavigationEvent(it)
                    }
                }
            }
        }
    }

    /**
     * Handles navigation events by navigating to the specified destination.
     *
     * @param navDirections The directions to navigate to.
     */
    private fun handleNavigationEvent(navDirections: NavDirections?) {
        navDirections?.let { dir ->
            findNavController(R.id.navigationContainer).navigate(dir)
        }
    }

    /**
     * Called when the user presses the "Up" button in the action bar.
     *
     * @return True if navigation was handled successfully, false otherwise.
     */
    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.navigationContainer)
        return navController.navigateUp()
                || super.onSupportNavigateUp()
    }
}