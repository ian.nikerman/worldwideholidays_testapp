package com.holidays.worldwideholidays.features.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import com.holidays.worldwideholidays.features.holidays.HolidaysFragmentDirections
import com.holidays.worldwideholidays.models.Holiday
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import timber.log.Timber

/**
 * ViewModel for the main screen, responsible for coordinating navigation events.
 */
class MainViewModel : ViewModel() {

    // Shared flow for emitting navigation events
    private val _navigationEvent = initNavigationEvent()

    // Public shared flow to observe navigation events
    val navigationEvent = _navigationEvent.asSharedFlow()

    /**
     * Initiates the navigation to the details screen for the selected holiday.
     *
     * @param item The selected holiday item to display details for.
     */
    fun openHoliday(item: Holiday) {
        Timber.tag(TAG).d(::openHoliday.name)
        viewModelScope.launch {
            // Emitting the navigation event using the shared flow
            _navigationEvent.emit(
                HolidaysFragmentDirections.actionHolidaysToDetails(item)
            )
        }
    }

    companion object {
        private val TAG = MainViewModel::class.java.simpleName

        /**
         * Initializes the shared flow for emitting navigation events.
         */
        private fun initNavigationEvent() = MutableSharedFlow<NavDirections?>()
    }
}
