package com.holidays.worldwideholidays.common.extensions

import com.holidays.worldwideholidays.models.Holiday
import com.holidays.worldwideholidays.models.HolidayData

/**
 * Extension functions for common operations and calculations.
 */
object CommonExtensions {

    /** Converting the Holiday model from the server to the UI model */
    fun List<HolidayData>?.getHolidaysUiModels() =
        this?.map { holidayData ->
            Holiday(
                id = getUniqueId(holidayData),
                isFavorite = false,
                date = holidayData.date,
                localName = holidayData.localName ?: "",
                name = holidayData.name,
                countryCode = holidayData.countryCode,
                fixed = holidayData.fixed,
                global = holidayData.global,
                counties = holidayData.counties?.joinToString() ?: "Empty",
                launchYear = holidayData.launchYear?.toString() ?: "Empty",
                types = holidayData.types.joinToString()
            )
        } ?: emptyList()

    /** Creating unique id for each holiday from object hashcode. */
    private fun getUniqueId(holidayData: HolidayData): Int {
        return holidayData.hashCode()
    }

    /** We change the isFavorite parameter for the holidays list, checking if the object is in the list from the local database. */
    fun List<Holiday>.updateFavoriteHolidays(favoritesHolidays: List<Holiday>?): List<Holiday> {
        val favoriteIds = favoritesHolidays?.map { it.id } ?: emptyList()
        return map { holiday ->
            holiday.copy(
                isFavorite = favoriteIds.contains(holiday.id)
            )
        }
    }
}