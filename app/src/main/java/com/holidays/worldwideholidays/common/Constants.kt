package com.holidays.worldwideholidays.common

/**
 * Constants class containing application-wide constant values.
 */
class Constants {

    companion object {

        // Base URL for the holiday server API
        const val BASE_SERVER_URL = "https://date.nager.at/api/v3/"

        // Local database name
        const val LOCAL_DATABASE = "local-database"

        // Delay duration for scrolling to the top in milliseconds
        const val DELAY_SCROLL_TO_TOP = 500L

        // Update interval for location updates in seconds
        const val UPDATE_INTERVAL_SECS = 5L

        // Minimum distance for location updates in meters
        const val UPDATE_DISTANCE_MIN = 200f

        // Maximum number of results from the geocoder service
        const val GEOCODER_MAX_RESULTS = 5
    }
}
