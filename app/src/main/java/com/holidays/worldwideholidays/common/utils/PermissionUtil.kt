package com.holidays.worldwideholidays.common.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import androidx.core.app.ActivityCompat

/**
 * Utility class for simplifying common permission and location-related tasks.
 */
object PermissionUtil {

    /**
     * Checks if the specified permission is granted.
     *
     * @param permission The permission to check.
     * @return `true` if the permission is granted, otherwise `false`.
     */
    fun Context.hasPermission(permission: String): Boolean {
        return ActivityCompat.checkSelfPermission(
            this,
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }

    /**
     * Checks if the location permission is denied and should be shown rationale.
     *
     * @return `true` if location permission is denied and rationale should be shown, otherwise `false`.
     */
    fun Activity.isLocationPermissionDenied(): Boolean {
        return ActivityCompat.shouldShowRequestPermissionRationale(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    }

    /**
     * Checks if GPS is enabled on the device.
     *
     * @return `true` if GPS is enabled, otherwise `false`.
     */
    fun Context.isGpsEnabled(): Boolean {
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }
}
