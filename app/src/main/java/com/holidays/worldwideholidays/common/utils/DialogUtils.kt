package com.holidays.worldwideholidays.common.utils

import android.Manifest
import android.content.Context
import android.content.Intent
import android.provider.Settings
import androidx.activity.result.ActivityResultLauncher
import androidx.appcompat.app.AlertDialog
import com.holidays.worldwideholidays.R

/**
 * Utility class for displaying common dialogs.
 */
object DialogUtils {

    /**
     * Prompts the user to enable GPS and opens system settings if confirmed.
     *
     * @param context The context to display the dialog.
     */
    fun promptEnableGps(context: Context) {
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setMessage(context.getString(R.string.enable_gps_message))
            .setPositiveButton(context.getString(R.string.enable_gps_positive_button)) { _, _ ->
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                context.startActivity(intent)
            }
            .setNegativeButton(context.getString(R.string.enable_gps_negative_button)) { _, _ ->
                // Negative button click handling, if needed
            }
        builder.create().show()
    }

    /**
     * Shows a dialog to ask the user for permission to access fine location.
     *
     * @param context The context to display the dialog.
     * @param permissionRequest The launcher for requesting location permission.
     */
    fun showLocationRationaleDialog(
        context: Context, permissionRequest: ActivityResultLauncher<String>
    ) {
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setMessage(context.getString(R.string.alert_dialog_permission_location_message))
            .setPositiveButton(context.getString(R.string.alert_dialog_permission_positive_button)) { dialog, _ ->
                // Launch the permission request if positive button is clicked
                permissionRequest.launch(Manifest.permission.ACCESS_FINE_LOCATION)
                dialog.dismiss()
            }
            .setNegativeButton(context.getString(R.string.alert_dialog_permission_negative_button)) { dialog, _ ->
                // Dismiss the dialog if negative button is clicked
                dialog.dismiss()
            }
        builder.create().show()
    }
}
