package com.holidays.worldwideholidays

import android.app.Application
import com.holidays.worldwideholidays.di.appModule
import com.holidays.worldwideholidays.di.databaseModule
import com.holidays.worldwideholidays.di.servicesModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.logger.Level
import timber.log.Timber

/**
 * Custom Application class for the Holidays App.
 *
 * This class extends the Android Application class and is responsible for initializing
 * application-wide components, such as Timber logger and Koin dependency injection.
 */
class HolidaysApp : Application() {

    /**
     * Called when the application is starting.
     */
    override fun onCreate() {
        super.onCreate()
        setupLogger()
        setupKoin()
    }

    /**
     * Sets up Timber logger for logging during development.
     */
    private fun setupLogger() {
        Timber.plant(Timber.DebugTree())
    }

    /**
     * Sets up Koin dependency injection framework.
     */
    private fun setupKoin() {
        startKoin {
            androidContext(this@HolidaysApp)
            androidLogger(if (BuildConfig.DEBUG) Level.ERROR else Level.NONE)
            modules(servicesModule, databaseModule, appModule)
        }
    }

    /**
     * Called when the application is terminated.
     */
    override fun onTerminate() {
        super.onTerminate()
        stopKoin()
    }
}