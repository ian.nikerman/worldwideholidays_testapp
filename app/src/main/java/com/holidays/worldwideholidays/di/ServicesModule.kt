package com.holidays.worldwideholidays.di

import android.content.Context
import android.content.res.Resources
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.gson.GsonBuilder
import com.holidays.worldwideholidays.common.Constants.Companion.BASE_SERVER_URL
import com.holidays.worldwideholidays.common.dispatchers.DefaultDispatcherProvider
import com.holidays.worldwideholidays.common.dispatchers.DispatcherProvider
import com.holidays.worldwideholidays.network.ApiService
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

/**
 * Koin module for providing various services used in the application.
 *
 * This module includes the provision of resources, a DispatcherProvider with default dispatchers,
 * a FusedLocationProviderClient for location services, an OkHttpClient for network requests,
 * Retrofit for building the HTTP client, and ApiService for defining API endpoints.
 */
val servicesModule = module {
    single { provideResources(get()) }
    single { provideDispatcherProvider() }
    single { provideFusedLocationProviderClient(get()) }
    single { provideDefaultOkHttpClient() }
    single { provideRetrofit(get()) }
    single { provideApiService(get()) }
}

/**
 * Provides Android resources.
 *
 * @param context The application context.
 * @return Resources instance.
 */
fun provideResources(context: Context): Resources {
    return context.resources
}

/**
 * Provides a DispatcherProvider with default dispatchers for various use cases.
 *
 * @return A DispatcherProvider instance.
 */
fun provideDispatcherProvider(): DispatcherProvider =
    DefaultDispatcherProvider()

/**
 * Provides a FusedLocationProviderClient for accessing location services.
 *
 * @param context The application context.
 * @return FusedLocationProviderClient instance.
 */
fun provideFusedLocationProviderClient(context: Context): FusedLocationProviderClient {
    return LocationServices.getFusedLocationProviderClient(context)
}

/**
 * Provides a default OkHttpClient with logging for network requests.
 *
 * @return OkHttpClient instance.
 */
fun provideDefaultOkHttpClient(): OkHttpClient {
    val logging = HttpLoggingInterceptor()
    logging.level = HttpLoggingInterceptor.Level.BODY

    val httpClient = OkHttpClient.Builder().addInterceptor(logging)
    return httpClient.build()
}

/**
 * Provides a Retrofit instance for building the HTTP client.
 *
 * @param client OkHttpClient instance.
 * @return Retrofit instance.
 */
fun provideRetrofit(client: OkHttpClient): Retrofit {
    val gson =
        GsonBuilder().create()

    return Retrofit.Builder().baseUrl(BASE_SERVER_URL).client(client)
        .addConverterFactory(ScalarsConverterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(CoroutineCallAdapterFactory()).build()
}

/**
 * Provides an implementation of the ApiService interface for defining API endpoints.
 *
 * @param retrofit Retrofit instance.
 * @return ApiService implementation.
 */
fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)