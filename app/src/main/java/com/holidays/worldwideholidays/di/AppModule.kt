package com.holidays.worldwideholidays.di

import com.holidays.worldwideholidays.data.DatabaseRepository
import com.holidays.worldwideholidays.data.HolidaysRepository
import com.holidays.worldwideholidays.data.LocationRepository
import com.holidays.worldwideholidays.features.holidays.HolidaysViewModel
import com.holidays.worldwideholidays.features.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Koin module for providing application-wide dependencies.
 *
 * This Koin module defines and configures dependencies used throughout the application.
 * It includes the provision of single instances for repositories like DatabaseRepository,
 * LocationRepository, and HolidaysRepository. Additionally, it provides view models
 * associated with various app features, enhancing the dependency management within the app.
 */
val appModule = module {

    // Repository dependencies
    single { DatabaseRepository(get()) }
    single { LocationRepository(get(), get()) }
    single { HolidaysRepository(get()) }

    // View model dependencies
    /** MainViewModel for the main feature of the app. */
    viewModel { MainViewModel() }

    /** HolidaysViewModel for the holidays feature of the app. */
    viewModel { HolidaysViewModel(get(), get(), get(), get(), get()) }

}
