package com.holidays.worldwideholidays.di

import androidx.room.Room
import com.holidays.worldwideholidays.common.Constants.Companion.LOCAL_DATABASE
import com.holidays.worldwideholidays.data.database.LocalDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val databaseModule = module {
    single {
        Room.databaseBuilder(androidContext(), LocalDatabase::class.java, LOCAL_DATABASE).build()
    }
    single { get<LocalDatabase>().getHolidayDao() }
}