package com.holidays.worldwideholidays.data

import com.holidays.worldwideholidays.data.database.HolidayDao
import com.holidays.worldwideholidays.models.Holiday
import kotlinx.coroutines.flow.Flow

/**
 * Repository class for interacting with the local Room Database to manage favorite holidays.
 *
 * This class provides methods to perform CRUD (Create, Read, Update, Delete) operations on
 * favorite holidays using the [HolidayDao].
 *
 * @property userDao The Data Access Object (DAO) for holiday-related database operations.
 */
class DatabaseRepository(private val userDao: HolidayDao) {

    /**
     * Retrieves a Flow of all favorite holidays from the local Room Database.
     *
     * @return A Flow emitting a list of favorite holidays whenever the data changes.
     */
    fun getAllFavoriteHolidays(): Flow<List<Holiday>> {
        return userDao.getAllFavoriteHolidays()
    }

    /**
     * Checks if a holiday with the specified ID is marked as a favorite.
     *
     * @param itemId The ID of the holiday to check.
     * @return `true` if the holiday is a favorite, `false` otherwise.
     */
    fun isHolidayFavorite(itemId: Int): Boolean {
        return userDao.isHolidayFavorite(itemId)
    }

    /**
     * Removes a holiday from the favorites based on its ID.
     *
     * @param itemId The ID of the holiday to be removed from favorites.
     */
    fun removeFavoriteHoliday(itemId: Int) {
        return userDao.removeFavoriteHoliday(itemId)
    }

    /**
     * Adds a new holiday to the list of favorites in the local Room Database.
     *
     * @param holiday The holiday to be added as a favorite.
     */
    fun addFavoriteHoliday(holiday: Holiday) {
        return userDao.addFavoriteHoliday(holiday)
    }
}
