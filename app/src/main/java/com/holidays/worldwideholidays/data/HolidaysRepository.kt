package com.holidays.worldwideholidays.data

import com.holidays.worldwideholidays.models.HolidayData
import com.holidays.worldwideholidays.models.ResponseState
import com.holidays.worldwideholidays.network.ApiService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import timber.log.Timber

/**
 * Repository class for managing the retrieval of holidays from the server.
 *
 * This class provides methods to fetch holidays, either from the server or the cached list,
 * based on the specified year and country code.
 *
 * @property apiService The service responsible for making API calls to retrieve holiday data.
 */
class HolidaysRepository(
    private val apiService: ApiService
) {

    /**
     * Fetches holidays based on the specified year and country code.
     *
     * @param year The year for which holidays are to be fetched.
     * @param countryCode The country code for which holidays are to be fetched.
     * @param isUpdate Flag indicating whether to force an update from the server.
     * @return A Flow emitting the response state of the holiday data.
     */
    suspend fun fetchHolidays(
        year: Int, countryCode: String, isUpdate: Boolean
    ): Flow<ResponseState<List<HolidayData>>> {
        return when {
            isUpdate || holidays.isNullOrEmpty() -> fetchHolidaysFromServer(year, countryCode)
            else -> flowOf(ResponseState.success(holidays))
        }
    }

    /**
     * Fetches holidays from the server based on the specified year and country code.
     *
     * @param year The year for which holidays are to be fetched.
     * @param countryCode The country code for which holidays are to be fetched.
     * @return A Flow emitting the response state of the holiday data.
     */
    private suspend fun fetchHolidaysFromServer(
        year: Int, countryCode: String
    ): Flow<ResponseState<List<HolidayData>>> {
        Timber.tag(TAG)
            .d("${::fetchHolidaysFromServer.name} with year: $year, countryCode: $countryCode")
        return flow {
            holidays = apiService.getHolidays(year, countryCode).body()
            Timber.tag(TAG).d("${::fetchHolidaysFromServer.name} success : $holidays")
            emit(ResponseState.success(holidays))
        }
    }

    companion object {
        private val TAG = HolidaysRepository::class.java.simpleName

        // Cached list of holidays
        private var holidays: List<HolidayData>? = null
    }
}