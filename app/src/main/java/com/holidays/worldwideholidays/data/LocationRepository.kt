package com.holidays.worldwideholidays.data

import android.annotation.SuppressLint
import android.content.Context
import android.location.Geocoder
import android.os.Looper
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.Granularity
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.Priority
import com.holidays.worldwideholidays.common.Constants.Companion.GEOCODER_MAX_RESULTS
import com.holidays.worldwideholidays.common.Constants.Companion.UPDATE_DISTANCE_MIN
import com.holidays.worldwideholidays.common.Constants.Companion.UPDATE_INTERVAL_SECS
import com.holidays.worldwideholidays.models.LocationData
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*
import timber.log.Timber
import java.io.IOException
import java.util.Locale

/**
 * Repository class for handling location-related operations.
 *
 * @property context The application context.
 * @property client The FusedLocationProviderClient for obtaining location updates.
 */
class LocationRepository(
    private val context: Context,
    private val client: FusedLocationProviderClient
) {

    private lateinit var callBack: LocationCallback

    /**
     * Retrieves location updates as a Flow of LocationData.
     *
     * @return Flow<LocationData> representing location updates.
     */
    @SuppressLint("MissingPermission")
    suspend fun getLocations(): Flow<LocationData> = callbackFlow {
        Timber.tag(TAG).d(::getLocations.name)
        val locationRequest =
            LocationRequest.Builder(Priority.PRIORITY_HIGH_ACCURACY, UPDATE_INTERVAL_SECS).apply {
                setMinUpdateDistanceMeters(UPDATE_DISTANCE_MIN)
                setGranularity(Granularity.GRANULARITY_PERMISSION_LEVEL)
                setWaitForAccurateLocation(true)
            }.build()

        callBack = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                Timber.tag(TAG).d("${::onLocationResult.name} : success")
                val location = locationResult.lastLocation
                val userLocation = LocationData(
                    latitude = location?.latitude ?: 0.0,
                    longitude = location?.longitude ?: 0.0
                )
                trySend(userLocation)
            }
        }

        client.requestLocationUpdates(locationRequest, callBack, Looper.getMainLooper())
        awaitClose { client.removeLocationUpdates(callBack) }
    }

    /**
     * Stops location updates.
     */
    fun stopLocationUpdates() {
        if (::callBack.isInitialized) {
            client.removeLocationUpdates(callBack)
        }
    }

    /**
     * Retrieves the country code based on the provided location.
     *
     * @param location The location for which to retrieve the country code.
     * @return The country code corresponding to the provided location.
     */
    fun getCountryCode(location: LocationData): String {
        val geoCoder = Geocoder(context, Locale.getDefault())

        try {
            val addresses =
                geoCoder.getFromLocation(
                    location.latitude,
                    location.longitude,
                    GEOCODER_MAX_RESULTS
                )
            Timber.tag(TAG).d("${::getCountryCode.name} succeeded")
            if (!addresses.isNullOrEmpty()) {
                val returnAddress = addresses.firstOrNull { !it.countryCode.isNullOrEmpty() }
                return returnAddress?.countryCode ?: ""
            }
        } catch (ex: IOException) {
            Timber.tag(TAG).d("${::getCountryCode.name} failed: $ex")
        }
        return ""
    }

    companion object {
        private val TAG = LocationRepository::class.java.simpleName
    }
}