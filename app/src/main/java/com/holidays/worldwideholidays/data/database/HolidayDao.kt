package com.holidays.worldwideholidays.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.holidays.worldwideholidays.models.Holiday
import kotlinx.coroutines.flow.Flow

/**
 * Data Access Object (DAO) for performing database operations related to holidays.
 *
 * This DAO interface defines methods to interact with the "holiday" table in the Room Database.
 */
@Dao
interface HolidayDao {

    /**
     * Retrieves a Flow of all favorite holidays from the database.
     *
     * @return A Flow emitting a list of favorite holidays whenever the data changes.
     */
    @Query("SELECT * FROM holiday")
    fun getAllFavoriteHolidays(): Flow<List<Holiday>>

    /**
     * Checks if a holiday with the specified ID is marked as a favorite.
     *
     * @param itemId The ID of the holiday to check.
     * @return `true` if the holiday is a favorite, `false` otherwise.
     */
    @Query("SELECT * FROM holiday WHERE id = :itemId")
    fun isHolidayFavorite(itemId: Int): Boolean

    /**
     * Removes a holiday from the favorites based on its ID.
     *
     * @param itemId The ID of the holiday to be removed from favorites.
     */
    @Query("DELETE FROM holiday WHERE id = :itemId")
    fun removeFavoriteHoliday(itemId: Int)

    /**
     * Inserts a new favorite holiday into the database.
     *
     * @param holiday The holiday to be added as a favorite.
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addFavoriteHoliday(holiday: Holiday)
}
