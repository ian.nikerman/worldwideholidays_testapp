package com.holidays.worldwideholidays.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.holidays.worldwideholidays.models.Holiday

/**
 * Room Database class for handling local storage of holiday data.
 *
 * This database includes a single table for storing holiday information.
 *
 * @property entities An array of entity classes (e.g., [Holiday::class]) representing the tables in the database.
 * @property version The version number of the database. Incrementing this number triggers a migration process when necessary.
 * @property exportSchema Whether to export the schema to a folder. Set to false to avoid a build warning.
 */
@Database(entities = [Holiday::class], version = 1, exportSchema = false)
abstract class LocalDatabase : RoomDatabase() {

    /**
     * Provides access to the Data Access Object (DAO) for interacting with the holiday table.
     *
     * @return An instance of [HolidayDao] for performing database operations related to holidays.
     */
    abstract fun getHolidayDao(): HolidayDao
}
