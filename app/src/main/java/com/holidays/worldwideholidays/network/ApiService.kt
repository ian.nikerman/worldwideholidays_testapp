package com.holidays.worldwideholidays.network

import com.holidays.worldwideholidays.models.HolidayData
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Retrofit service interface for making API calls related to holiday data.
 *
 * This interface defines methods for fetching holiday data from the server.
 */
interface ApiService {

    /**
     * Fetches holiday data for a specific year and country code.
     *
     * @param year The year for which holiday data is requested.
     * @param countryCode The country code for which holiday data is requested.
     * @return A Retrofit [Response] containing a list of [HolidayData].
     */
    @GET("publicholidays/{year}/{countryCode}")
    suspend fun getHolidays(
        @Path("year") year: Int,
        @Path("countryCode") countryCode: String
    ): Response<List<HolidayData>>

}
