package com.holidays.worldwideholidays.models

/**
 * Represents the current state of a data operation along with associated data and messages.
 *
 * @param T The type of data associated with the state.
 * @property status The status of the data operation.
 * @property data The associated data (can be `null`).
 * @property message A message providing additional information (can be `null`).
 */
data class ResponseState<out T>(val status: Status, val data: T?, val message: String?) {

    companion object {

        /**
         * Creates a [ResponseState] with an [Status.EMPTY] status.
         */
        fun <T> onEmpty(): ResponseState<T> {
            return ResponseState(Status.EMPTY, null, "")
        }

        /**
         * Creates a [ResponseState] with a [Status.SUCCESS] status and the provided data.
         */
        fun <T> success(data: T?): ResponseState<T> {
            return ResponseState(Status.SUCCESS, data, null)
        }

        /**
         * Creates a [ResponseState] with a [Status.ERROR] status and the provided error message.
         */
        fun <T> error(msg: String): ResponseState<T> {
            return ResponseState(Status.ERROR, null, msg)
        }

        /**
         * Creates a [ResponseState] with a [Status.LOADING] status.
         */
        fun <T> loading(): ResponseState<T> {
            return ResponseState(Status.LOADING, null, null)
        }
    }
}

/**
 * Enum representing the possible statuses of a data operation.
 */
enum class Status {
    EMPTY, SUCCESS, ERROR, LOADING
}