package com.holidays.worldwideholidays.models

/**
 * Data class representing geographic coordinates (latitude and longitude).
 *
 * @property latitude The latitude coordinate.
 * @property longitude The longitude coordinate.
 */
data class LocationData(
    val latitude: Double,
    val longitude: Double
)