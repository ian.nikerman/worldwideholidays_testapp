package com.holidays.worldwideholidays.models

/**
 * Data class representing information about a holiday.
 *
 * @property date The date of the holiday.
 * @property localName The local name of the holiday (nullable).
 * @property name The name of the holiday.
 * @property countryCode The country code associated with the holiday.
 * @property fixed Indicates if the holiday is fixed.
 * @property global Indicates if the holiday is global.
 * @property counties The list of counties where the holiday is observed (nullable).
 * @property launchYear The launch year of the holiday (nullable).
 * @property types The list of types associated with the holiday.
 */
data class HolidayData(
    val date: String,
    val localName: String?,
    val name: String,
    val countryCode: String,
    val fixed: Boolean,
    val global: Boolean,
    val counties: List<String>?,
    val launchYear: Int?,
    val types: List<String>
)