package com.holidays.worldwideholidays.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

/**
 * Represents a holiday entity with details stored in the local database.
 *
 * @param id The unique identifier for the holiday.
 * @param isFavorite Indicates whether the holiday is marked as a favorite.
 * @param date The date of the holiday.
 * @param localName The local name of the holiday.
 * @param name The name of the holiday.
 * @param countryCode The country code associated with the holiday.
 * @param fixed Indicates whether the holiday has a fixed date.
 * @param global Indicates whether the holiday is global.
 * @param counties The counties associated with the holiday.
 * @param launchYear The launch year of the holiday.
 * @param types The types of the holiday.
 */
@Parcelize
@Entity(tableName = "holiday")
data class Holiday(
    @PrimaryKey val id: Int,
    val isFavorite: Boolean,
    val date: String,
    val localName: String,
    val name: String,
    val countryCode: String,
    val fixed: Boolean,
    val global: Boolean,
    val counties: String,
    val launchYear: String,
    val types: String
) : Parcelable