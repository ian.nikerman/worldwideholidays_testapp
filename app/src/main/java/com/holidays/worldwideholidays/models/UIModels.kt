package com.holidays.worldwideholidays.models

/**
 * Enum representing the possible states for a list of holidays.
 */
enum class ListState {
    HOLIDAYS, FAVORITES
}

/**
 * Represents the data structure for a list of holidays.
 *
 * @param holidays The list of holidays.
 * @param scrollToTop Indicates whether to scroll to the top of the list.
 * @param emptyListTitle The title to display when the list is empty.
 */
data class ListData(
    val holidays: List<Holiday>?,
    val scrollToTop: Boolean,
    val emptyListTitle: String?,
)